﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(NPCPath))]
public class NPCPathEditor : Editor
{
    public override void OnInspectorGUI()
    {
        NPCPath path = (NPCPath)target;
        DrawDefaultInspector();
        if (GUILayout.Button("GetTransformsInPath"))
        {
            path.PutNodes();
        }
        
    }
}
