﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour
{

    public AudioSource mainMenuMusic;
    // Start is called before the first frame update
    void Start()
    {
        mainMenuMusic.time = 29.0f;
        mainMenuMusic.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
