﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathSkyAlert : MonoBehaviour
{
    Canvas _cnsAlertCanvas;
    public Text txtDangerAlert;
    public Image imgDanger;
    public bool _blnActive = false;
    public float fltRatio;
    float fltT;
    public AudioSource DangerSound;
    private void Start()
    {
        _cnsAlertCanvas = GetComponentInChildren<Canvas>();
    }
    private void Update()
    {
        if (_blnActive)
        {
            AlternateFunction();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            DangerSound.Play();
            _blnActive = true;
            _cnsAlertCanvas.enabled = true;
            imgDanger.enabled = true;

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            DangerSound.Stop();
            _blnActive = false;
            _cnsAlertCanvas.enabled = false;
            imgDanger.enabled = false;
        }
    }
    void AlternateFunction()
    {
        if (fltT >= fltRatio)
        {
            //_cnsAlertCanvas.enabled = !_cnsAlertCanvas.enabled;
            txtDangerAlert.enabled = !txtDangerAlert.enabled;
            fltT = 0.0f;
        }
        fltT += Time.deltaTime;
    }
}
