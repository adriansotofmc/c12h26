﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveFile
{
    [System.Serializable]
    public struct PlayerAndScore
    {
        public string strName;
        public int intMinutos;
        public int intSegundos;
        public int intMilesimas;
    }

    public PlayerAndScore[] SavedScores;

    public List<Vector3> BestPosition;
    public List<Vector3> BestRotation;

    public SaveFile(List<PlayerAndScore> _Scores, int intScoresToSave)
    {
        if (_Scores.Count < intScoresToSave)
        {
            SavedScores = new PlayerAndScore[_Scores.Count];
        }
        else
        {
            SavedScores = new PlayerAndScore[intScoresToSave];
        }
 

        for (int i = 0; i < intScoresToSave; i++)
        {
            if (_Scores.Count <= i)
            {
                break;
            }
            SavedScores[i] = _Scores[i];

        }
    }
}
