﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class ScoreBoard
{
    static List<SaveFile.PlayerAndScore> _times = new List<SaveFile.PlayerAndScore>();
    public static int TimesToSave = 10;

    public static List<Vector3> BestPosition;
    public static List<Vector3> BestRotation;


    public static void AddTime(string _strName, int _intMinutos, int _intSegundos, int _intMilesimas)//add a new time to the list of playerandscore(better to do it organized)
    {
        SaveFile.PlayerAndScore _currentScore = new SaveFile.PlayerAndScore();
        _currentScore.strName = _strName;
        _currentScore.intMinutos = _intMinutos;
        _currentScore.intSegundos = _intSegundos;
        _currentScore.intMilesimas = _intMilesimas;

        if (_times.Count == 0)
        {
            _times.Insert(0, _currentScore);
        }
        else
        {
            //go throught the list comparing the minutes of the savescores. if the minutes of the new time are less then the ones its comparing to, put the time in the index of that, if its the same compare the second, if the same the milisecons. 
            //if at any point  it is more, go to the next slot
            bool blnAdded = false;
            for (int i = 0; i < _times.Count; i++)//we go throught it from the frist place
            {
                if (_times[i].intMinutos > _currentScore.intMinutos)//if the time is lower in seconds we place it there
                {
                    _times.Insert(i, _currentScore);
                    blnAdded = true;
                    break;
                }
                else if (_times[i].intMinutos == _currentScore.intMinutos)//if the minutes are the same we compare the seconds
                {
                    if (_times[i].intSegundos > _currentScore.intSegundos)//if the second are lowe we insert the time in place
                    {
                        _times.Insert(i, _currentScore);
                        blnAdded = true;
                        break;
                    }
                    else if (_times[i].intSegundos == _currentScore.intSegundos)//if the second are the same we compare the miliseconds
                    {
                        if (_times[i].intMilesimas > _currentScore.intMilesimas)//if the mili are lower we place it
                        {
                            blnAdded = true;
                            _times.Insert(i, _currentScore);
                            break;
                        }
                    }
                }
            }
            if (!blnAdded)
            {
                _times.Insert(_times.Count, _currentScore);
            }
        }
    }
    public static List<SaveFile.PlayerAndScore> ShowTimes()// Show an organized list of the saved playerandscore by time 
    {
        return _times;
    }
    public static string ShowTimesFormated()//show a string of the times formated to be put in a scoreboard
    {
        return "lol";
    }
    public static bool BetterThanBestTime()
    {
        if (_times.Count > 0)
        {
            if (StaticClassLoad.Minutes < _times[0].intMinutos)
            {
                return true;
            }
            else if (StaticClassLoad.Minutes == _times[0].intMinutos)
            {
                if (StaticClassLoad.Segundos < _times[0].intSegundos)
                {
                    return true;
                }
                else if (StaticClassLoad.Segundos == _times[0].intSegundos)
                {
                    if (StaticClassLoad.Milesimas < _times[0].intMilesimas)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        else
        {
            return true;
        }

    }

    public static void SaveData(string strFileName)//serialize a json of a savefile(put the playerandScore list in a savefile and serialize it)
    {
        SaveFile _file = new SaveFile(_times, TimesToSave);
        _file.BestPosition = BestPosition;
        _file.BestRotation = BestRotation;
        string json = JsonUtility.ToJson(_file);
        File.WriteAllText(Application.dataPath + "/" + strFileName + ".txt", json);
    }

    public static void LoadData(string strFileName)//get the data of a savefile and store it in the list
    {
        if (File.Exists(Application.dataPath + "/"+strFileName+".txt"))//save file too complex to save
        {
            try
            {
                string _jsonToLoad = File.ReadAllText(Application.dataPath + "/" + strFileName + ".txt");
                SaveFile save = JsonUtility.FromJson<SaveFile>(_jsonToLoad);
                _times.Clear();
                Debug.Log(save.SavedScores.Length + " data scores");
                for (int i = 0; i < save.SavedScores.Length; i++)
                {
                    AddTime(save.SavedScores[i].strName, save.SavedScores[i].intMinutos, save.SavedScores[i].intSegundos, save.SavedScores[i].intMilesimas);
                }

                StaticClassLoad.PreviousPositions = save.BestPosition;
                StaticClassLoad.PreviousRotations = save.BestRotation;
                BestRotation = save.BestRotation;
                BestPosition = save.BestPosition;

                Debug.Log(ScoreBoard.ShowTimes().Count + " loaded scores");
                for (int i = 0; i < ScoreBoard.ShowTimes().Count; i++)
                {
                    Debug.Log(ScoreBoard.ShowTimes()[i].strName + "----" + ScoreBoard.ShowTimes()[i].intMinutos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intSegundos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intMilesimas.ToString());
                }
            }
            catch
            {
                Debug.LogError("Failed To load Save file (formating error?)");
            }
        }
        else
        {
            Debug.LogError("File name doesnt exist");
        }
    }



}
