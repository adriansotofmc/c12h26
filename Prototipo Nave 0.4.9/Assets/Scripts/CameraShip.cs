﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShip : MonoBehaviour
{
    public float fltSpeed;

    [Header("NO TOUCHING")]
    public Transform trTarget;
    public Transform trParent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Follow();
    }

    void Follow()
    {
        var v3Direction = new Vector3();
        v3Direction = Vector3.Normalize(trTarget.position - transform.position);
        transform.forward = v3Direction;
        transform.position += Vector3.Normalize(trParent.position - transform.position) * fltSpeed * Time.deltaTime;
    }
    //////////////////////////////////////-----------------this is useless---------------------/////////////////////////////////////
}
