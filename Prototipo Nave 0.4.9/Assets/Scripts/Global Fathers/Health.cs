﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [Header("Settings")]
    public float fltMaxHP;
    [Header("Current values")]
    public float fltCurrentHealth;
    // Start is called before the first frame update
   protected virtual void Start()
    {
        fltCurrentHealth = fltMaxHP;
    }

    public virtual void ChangeHP(float fltAddValue)
    {
        fltCurrentHealth += fltAddValue;
    }
}
