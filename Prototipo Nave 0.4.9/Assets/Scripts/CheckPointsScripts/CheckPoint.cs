﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public GameObject _player;
    public GameObject gm;
    protected bool _blnActive = false;//if the checpoint is one that is available
    MeshRenderer _mrMy;
    AchivementManager AM;

    [SerializeField] UICompass myCompass;

    [Header("Other Checkpoints")]
    public CheckPoint _cpBefore;
    public CheckPoint _cpAfter;

    [SerializeField]
    private AudioSource CPSound;

    private void Start()
    {
        CPSound.volume = 10;
        AM = FindObjectOfType<AchivementManager>();
        _mrMy = GetComponent<MeshRenderer>();
        _mrMy.enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            
            CPSound.Play();
            ShipPasses();
        }
        
    }

    protected virtual void ShipPasses()
    {
        if (_blnActive)
        {
            AM.CheckpointReached();
            gm.GetComponent<GameMaster>().SetActiveCheckPoint(this);
            DeactivatePoint();
            _cpAfter.ActivatePoint();
            myCompass.SetTargetCheckpoint(_cpAfter.transform);
        }
    }
    public void RestartFromCheckPoint()//we would need to improve the check point sistem so it changes the position better
    {//this needs improvement
        StartCoroutine(RestartC(0.2f));
        _player.GetComponent<ShipPlayerController>().ChecpointRestart(transform.position, transform.rotation * Quaternion.Euler(90.0f, -90.0f, 0.0f));
        //_player.transform.rotation = transform.rotation * Quaternion.Euler(90, 0.0f, 0.0f)
    }

    public void ActivatePoint()
    {
        _blnActive = true;
        _mrMy.enabled = true;
    }
    public void DeactivatePoint()
    {
        _blnActive = false;
        _mrMy.enabled = false;
    }
    IEnumerator RestartC(float fltTime)
    {
        Time.timeScale = 0.7f;
        yield return new WaitForSeconds(fltTime);
        Time.timeScale = 1.0f;
    }
}
