﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class TextColorChange : MonoBehaviour
{
    [SerializeField]
    private Text theText;

    public void OnMouseEnter(PointerEventData eventData)
    {
        theText.color = Color.blue; //Or however you do your color
    }

    public void OnMouseExit(PointerEventData eventData)
    {
        theText.color = Color.white; //Or however you do your color
    }
}

