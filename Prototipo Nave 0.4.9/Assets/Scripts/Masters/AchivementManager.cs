﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchivementManager : MonoBehaviour
{//this piece of shit is the thing that controls the achivenemts and stuff
    [Header("Help"), SerializeField]
    ShipPlayerController _Ship;
    public AnimationCurve IconCurve;
    public float fltAnimationTime;
    public RectTransform trmEndPosIcon;

    //Fearless
    DeathSkyAlert _dsa;
    [Header("Fearless")]
    public RectTransform trmFearlessGroup;
    public float fltFearlessTime;
    float fltDeathSkyTime;

    [Header("RiskTaker")]
    public RectTransform trmRiskTakerGroup;
    public float fltRiskTakerTime;
    float fltRTTime;

    [Header("SpeedDemon")]
    public RectTransform trmSpeedDemonGroup;
    public float fltSpeedDemonTime;
    float fltSDT;

    [Header("Clean")]
    public RectTransform trmCleanGroup;

    [Header("Mess")]
    public RectTransform trmMessGroup;

    bool blnClean = false;
    float fltPreviousHealth = 0.0f;
    bool blnMess = false;

    private void Awake()
    {
        _dsa = FindObjectOfType<DeathSkyAlert>();
        fltDeathSkyTime = 0.0f;
    }

    private void Update()//remember to put all the fucntions here
    {
        FearlessFunction();
        RiskTakerFunction();
        SpeedDemonFunction();
    }

    IEnumerator IconMoveCorru(RectTransform trmIcon)
    {
        Debug.Log("LaunchedCorru");
        Vector2 v2OgPos = trmIcon.anchoredPosition;
        float fltTime = 0.0f;
        while (fltTime < fltAnimationTime)
        {
            Debug.Log("Playing Anim");
            trmIcon.anchoredPosition = Vector2.Lerp(v2OgPos, trmEndPosIcon.anchoredPosition, IconCurve.Evaluate(fltTime / fltAnimationTime));
            yield return null;
            fltTime += Time.deltaTime;
        }
        while (fltTime > 0.0f)
        {
            Debug.Log("Playing Anim");
            trmIcon.anchoredPosition = Vector2.Lerp(trmEndPosIcon.anchoredPosition, v2OgPos, IconCurve.Evaluate(1 - (fltTime / fltAnimationTime)));
            yield return null;
            fltTime -= Time.deltaTime;
        }
        trmIcon.anchoredPosition = v2OgPos;
    }

    #region Fearless
    //aguarded to people who dont care about the danger zone
    void FearlessFunction()
    {
        if (_dsa._blnActive)
        {
            fltDeathSkyTime += Time.deltaTime;
            if (fltDeathSkyTime >= fltFearlessTime)
            {
                //the function to display the thing
                StartCoroutine(IconMoveCorru(trmFearlessGroup));
                Debug.Log("FEARLESS");
                fltDeathSkyTime -= fltFearlessTime;
            }
        }
        else
        {
            fltDeathSkyTime = 0.0f;
        }
    }
    #endregion

    #region  RiskTaker
    //aguarded to people that go close to wall for long periods of time
    void RiskTakerFunction()
    {
        if (_Ship.blnAuxiliarForAchivements)
        {
            fltRTTime += Time.deltaTime;
            if (fltRTTime >= fltRiskTakerTime)
            {
                //dysplay function
                StartCoroutine(IconMoveCorru(trmRiskTakerGroup));
                Debug.Log("RISKTAKER");
                fltRTTime -= fltRiskTakerTime;
            }
        }
        else
        {
            fltRTTime = 0.0f;
        }
    }
    #endregion

    #region SpeedDemon
    //people that go at max speed all the time
    void SpeedDemonFunction()
    {
        if (_Ship.fltSpeed >= 0.9f)
        {
            fltSDT += Time.deltaTime;
            if (fltSDT >= fltSpeedDemonTime)
            {
                //Display function
                StartCoroutine(IconMoveCorru(trmSpeedDemonGroup));
                Debug.Log("SPEEDDDEMON");
                fltSDT -= fltSpeedDemonTime;
            }
        }
        else
        {
            fltSDT = 0.0f;
        }
    }
    #endregion

    #region CleanDriver/Mess
    public void CheckpointReached()//needed for CleanDriver and Mess
    {
        Health _shipHealth = _Ship.GetComponent<Health>();
        if(_shipHealth.fltCurrentHealth== _shipHealth.fltMaxHP)//clean driver
        {
            if (blnClean)
            {
                //DisplayFunction
                StartCoroutine(IconMoveCorru(trmCleanGroup));
                Debug.Log("CLEANDRIVER");
            }
            blnClean = true;
        }
        //Mess
        if (fltPreviousHealth <= 0.3f * _shipHealth.fltMaxHP)
        {
            fltPreviousHealth = _shipHealth.fltCurrentHealth;
            if (blnMess)
            {
                //DisplayFunction
                StartCoroutine(IconMoveCorru(trmMessGroup));
                Debug.Log("MESS");
            }
            blnMess = true;
        }
    }
    #endregion
}
