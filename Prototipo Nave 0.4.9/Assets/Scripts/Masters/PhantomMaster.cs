﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhantomMaster : MonoBehaviour//this fucker is the one responsible for all the player phantoms and shit(Recording and displaying)
{//the saving shit will be stuff for another fucker
    [Header("Stats Of phantoms"), SerializeField,Tooltip("the amount of time in second between each time the position is recorded")]
    float fltPeriod;//this value must be the same between recording as it affects the speed of the Phantoms shown
    public GameObject goPlayerPhantom;
    public bool blnShowPhantom;//must be set before starting the circus
    public bool blnRecord;//must be set before starting the circus--probably before the race start

    public Transform trmPlayer;
    public GameObject goPhantomPrefab;

    Transform trmPhantomInstance;
    List<Vector3> PreviousPositions;// the recording of the previous best score
    List<Vector3> PreviousRotations;// the recording of the previous best score
    public List<Vector3> CurrentPositions;//the positions that are currently being recorded
    public List<Vector3> CurrentRotations;//the positions that are currently being recorded

    public void StartPhantoms()
    {
        if (SceneManager.GetActiveScene().buildIndex == 1)
        {
            ScoreBoard.LoadData("Level 2");
        }else if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            ScoreBoard.LoadData("Level 3");
        }
        PreviousPositions = StaticClassLoad.PreviousPositions;
        PreviousRotations = StaticClassLoad.PreviousRotations;
        if (blnRecord)
        {
            //corrutine
            StartCoroutine(RecordPlayerCorru());
        }
        if (blnShowPhantom)
        {
            try
            {
                //we instanciate the phantom and get a reference to its transform
                trmPhantomInstance = Instantiate(goPhantomPrefab, PreviousPositions[0], Quaternion.Euler(PreviousRotations[0])).GetComponent<Transform>();
                //corrutine
                StartCoroutine(ShowPhantomCorru());
            }
            catch
            {
                Debug.LogError("No Previous Phantoms");
            }
        }
    }

    IEnumerator ShowPhantomCorru()//this should update the position and rotation of the phantom 
    {
        int CurrentIndex = 0;
        float fltElapsedTime = 0;
        while (CurrentIndex < PreviousPositions.Count - 1)
        {
            fltElapsedTime += Time.deltaTime;
            //we put the phantom on the corresponding position and rotation
            trmPhantomInstance.position = Vector3.Lerp(PreviousPositions[CurrentIndex], PreviousPositions[CurrentIndex + 1], fltElapsedTime / fltPeriod);
            trmPhantomInstance.rotation = Quaternion.Euler(Vector3.Lerp(PreviousRotations[CurrentIndex], PreviousRotations[CurrentIndex + 1], fltElapsedTime / fltPeriod));
            if (fltElapsedTime >= fltPeriod)//if the period is done we move to the next pair
            {
                fltElapsedTime -= fltPeriod;//we reset the elapsed time but we keep the amount of time it went over the period just in case
                CurrentIndex++;//we move to the next pair of values
                trmPhantomInstance.rotation = Quaternion.Euler(PreviousRotations[CurrentIndex]);
            }
            yield return null;
        }
    }
    IEnumerator RecordPlayerCorru()//this fucker should record the movements of the player
    {
        while (true)
        {
            Debug.Log("AddedPosition");
            CurrentPositions.Add(trmPlayer.position);
            CurrentRotations.Add(trmPlayer.rotation.eulerAngles);
            yield return new WaitForSecondsRealtime(fltPeriod);
        }
    }
}
