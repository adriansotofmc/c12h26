﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    enum GameState
    {
        PreRace,
        CountDown,
        Race,
        Pause,
        Dead,
        PostRace
    }

    [SerializeField]
    GameState CurrentGameState;
    [SerializeField]
    private AudioSource EngineSound;
    [SerializeField]
    private AudioSource explosion;
    [SerializeField]
    private AudioSource raceMusic;

    private float targetPitch;
    public float incrementPitch = 0.5f;


    [Header("PreRaceMenuStuff")]
    [SerializeField]
    public Canvas _cvsPreRaceMenu;
    [SerializeField]
    public Camera _cPreRaceCamera;
    [Header("CountDownStuff")]
    [SerializeField]
    public Canvas _cvsCountDown;
    float _fltCurrentNumberCountDown = 0.5f;
    [Header("EndScene")]
    public float fltEndRaceTime;
    public Canvas cvsTransitionCanvas;
    public Image imgTransitionCanvas;
    [Header("PauseMenu")]
    public Canvas _cvsPauseCanvas;
    public float _fltPauseTimeScale;

    [Header("Other")]
    public Canvas _cvsGameUI;
    public Image imFader;
    public DeathSky _ds;
    public CronometroProvisional _crono;
    public GameObject _player;
    public CheckPoint cpFirstCheckPoint;
    public GameObject _deathParticles;
    public UIController _UIShit;
    //public CheckPoint[] List;//in order list of checkpoints
    private CheckPoint _cpActive;
    // Start is called before the first frame update
    void Start()
    {
        StaticClassLoad.RetryButtonIndex = SceneManager.GetActiveScene().buildIndex;
        raceMusic.time = 83.0f;
        raceMusic.volume = 0.1f;
        raceMusic.Play();
        ScoreBoard.LoadData(SceneManager.GetActiveScene().name);
        Time.timeScale = 1.0f;
        PreRaceEnterFunction();
        _cvsCountDown.enabled = false;
        _cvsGameUI.enabled = false;
        _ds._blnActive = false;
        StartCoroutine(Fader(0.2f));
        //CheckpointIni
        cpFirstCheckPoint.ActivatePoint();
        _cpActive = cpFirstCheckPoint;
        _fltCurrentNumberCountDown = 4.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) || (Input.GetAxis("Accel") > 0))
        {
            incrementPitch = 0.6f;
            targetPitch = 6.0f;
            EngineSound.pitch = Mathf.Lerp(EngineSound.pitch, targetPitch, incrementPitch * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.LeftControl) || (Input.GetAxis("Deccel") > 0))
        {
            incrementPitch = 1.5f;
            targetPitch = 0.0f;
            EngineSound.pitch = Mathf.Lerp(EngineSound.pitch, targetPitch, incrementPitch * Time.deltaTime);
        }



        switch (CurrentGameState)
        {
            case GameState.PreRace://before race (StartScreen)
                PreRaceState();
                break;
            case GameState.CountDown://countDown so that the race doesnt start the moment you press the button
                break;
            case GameState.Race://Durring race
                raceMusic.UnPause();
                EngineSound.UnPause();
                RaceFunction();
                break;
            case GameState.Pause://the game is in the pause screen
                PauseFunction();
                raceMusic.Pause();
                EngineSound.Pause();
                break;
            case GameState.Dead://The player fucking dies and its screen
                EngineSound.Stop();
                break;
            case GameState.PostRace://the race finishes
                
                EngineSound.Stop();
                EndRaceFunction();
                Debug.Log("fjfj");
                break;
            default:
                break;

        }
    }

    //preRace funcions-----------------------------
    IEnumerator Fader(float fltDuration)
    {
        float fltTime = 0.0f;
        while (fltTime < fltDuration)
        {
            imFader.color = new Color(imFader.color.r, imFader.color.g, imFader.color.b, 1 - fltTime / fltDuration);
            fltTime += Time.deltaTime;
            yield return null;
        }
        imFader.color = new Color(imFader.color.r, imFader.color.g, imFader.color.b, 0.0f);
        imFader.enabled = false;
    }

    void PreRaceEnterFunction()
    {
        CurrentGameState = GameState.PreRace;
        _player.GetComponentInChildren<Camera>().enabled = false;
        _cPreRaceCamera.enabled = true;
        _cvsPreRaceMenu.enabled = true;
        _crono._blnCounting = false;
        _player.GetComponent<ShipPlayerController>().ShipState = ShipPlayerController.States.Stopped;
    }


    void PreRaceState()//no se que poner aqui
    {
        EngineSound.Stop();
    }
    //CountDownFunctions-----------------------
    public void CountDownEnter()
    {
        EngineSound.Play();
        targetPitch = 0.2f;
        EngineSound.pitch = Mathf.Lerp(EngineSound.pitch, targetPitch, incrementPitch * Time.deltaTime);
        CurrentGameState = GameState.CountDown;
        _player.GetComponentInChildren<Camera>().enabled = true;
        _cPreRaceCamera.enabled = false;
        _cvsPreRaceMenu.enabled = false;
        _cvsPreRaceMenu.gameObject.SetActive(false);
        _cvsCountDown.enabled = true;
        _player.GetComponent<ShipPlayerController>().ShipState = ShipPlayerController.States.DuringCountDown;
        StartCoroutine(CountDownCor());
    }
    IEnumerator CountDownCor()//it works now
    {
        raceMusic.time = 106.0f;

        _cvsCountDown.GetComponentInChildren<Text>().text = "3";
        yield return new WaitForSeconds(1);
        _cvsCountDown.GetComponentInChildren<Text>().text = "2";
        yield return new WaitForSeconds(1);
        _cvsCountDown.GetComponentInChildren<Text>().text = "1";
        yield return new WaitForSeconds(1);
        _cvsCountDown.GetComponentInChildren<Text>().text = "GO";
        yield return new WaitForSeconds(1);
        RaceEnter();
    }
    //RacewFunctions----------------------
    void RaceEnter()
    {
        _cvsGameUI.enabled = true;
        _cvsCountDown.enabled = false;
        _ds._blnActive = true;
        _player.GetComponent<ShipPlayerController>().ShipState = ShipPlayerController.States.Normal;
        CurrentGameState = GameState.Race;
        _crono._blnCounting = true;

        GetComponent<PhantomMaster>().StartPhantoms();
    }

    void RaceFunction()//comprobations for dead and pause
    {
        if (Input.GetButtonDown("Pause"))
        {
            PauseEnter();
        }
    }

    //RaceEndStuff
    public void EndRace()
    {
        StartCoroutine(EndRaceCorru());
        Debug.Log("Hello");
        //End secuence of race
        CurrentGameState = GameState.PostRace;
        Time.timeScale = 0.1f;
        StaticClassLoad.RetryButtonIndex = SceneManager.GetActiveScene().buildIndex;
        //phantoms
        StaticClassLoad.CurrentPositions = GetComponent<PhantomMaster>().CurrentPositions;
        StaticClassLoad.CurrentRotations = GetComponent<PhantomMaster>().CurrentRotations;

        //_txtEndCrono.text= $"{_crono.Minutos}:{_crono.Segundos}:{_crono.Milesimas}";
        //_ds._blnActive = false;
        //_player.GetComponent<ShipPlayerController>().ShipState = ShipPlayerController.States.Pause;
        //_cvsGameUI.enabled = false;
        //_cvsEndRace.enabled = true;
    }

    private void EndRaceFunction()
    {
        _fltCurrentNumberCountDown -= Time.unscaledDeltaTime;
        if (_fltCurrentNumberCountDown <= 0)
        {
            //StaticClassLoad.SetTime(_crono.Minutos, _crono.Segundos, _crono.Milesimas);
            //StaticClassLoad.LoadIndex = 2;//change when more levels are added
            //SceneManager.LoadScene("LoadingScene");
        }
    }

    private IEnumerator EndRaceCorru()
    {
        //yield return new WaitForSecondsRealtime(1.5f);
        
        cvsTransitionCanvas.enabled = true;
        float fltTime = 0.0f;
        while (fltTime <= fltEndRaceTime)
        {
            // imgTransitionCanvas.fillAmount = fltTime / fltEndRaceTime;
            imgTransitionCanvas.color = new Vector4(imgTransitionCanvas.color.r, imgTransitionCanvas.color.g, imgTransitionCanvas.color.b, fltTime / fltEndRaceTime);
            fltTime += Time.unscaledDeltaTime;
            yield return null;
        }
        Time.timeScale = 1.0f;
        StaticClassLoad.SetTime(_crono.Minutos, _crono.Segundos, _crono.Milesimas);
        SceneManager.LoadScene("ResultScreen");
       // StaticClassLoad.LoadIndex = 3;//change when more levels are added
       // SceneManager.LoadScene("LoadingScene");
    }

    //Pause Stuff

    void PauseEnter()
    {
        CurrentGameState = GameState.Pause;
        Time.timeScale = _fltPauseTimeScale;
        _cvsPauseCanvas.gameObject.SetActive(true);//just in case becose of the random bug that gets you to the main menu 
        _cvsPauseCanvas.enabled = true;
        _player.GetComponent<ShipPlayerController>().ShipState = ShipPlayerController.States.Pause;
    }

    void PauseFunction()
    {
        if (Input.GetButtonDown("Pause"))
        {
            CurrentGameState = GameState.Race;
            Time.timeScale = 1.0f;
            _cvsPauseCanvas.enabled = false;
            _cvsPauseCanvas.gameObject.SetActive(false);
            _player.GetComponent<ShipPlayerController>().ShipState = ShipPlayerController.States.Normal;
        }
    }

    //pause buttons

    public void ResumeButton()
    {
        CurrentGameState = GameState.Race;
        Time.timeScale = 1.0f;
        _cvsPauseCanvas.enabled = false;
        _cvsPauseCanvas.gameObject.SetActive(true);
        _player.GetComponent<ShipPlayerController>().ShipState = ShipPlayerController.States.Normal;
    }

    public void RestartButton()
    {
        Debug.Log("Loading");
        Time.timeScale = 1.0f;
        StaticClassLoad.LoadIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene("LoadingScene");
    }

    public void BackToMainMenuButton()
    {
        Time.timeScale = 1.0f;
        StaticClassLoad.LoadIndex = 0;
        SceneManager.LoadScene("LoadingScene");
    }

    //funcions before rework
    void RestartFunction()
    {
        if (Input.GetButtonDown("Restart"))
        {
            _cpActive.RestartFromCheckPoint();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
    }

    public void RestartOnDead()//if the player fucking dies
    {
        StartCoroutine(death());
    }
    public void SetActiveCheckPoint(CheckPoint _cp)
    {
        //_UIShit._intCheckpointCount++;
        FindObjectOfType<UIController>()._intCheckpointCount++;
        _cpActive = _cp;
    }

    IEnumerator death()
    {
        explosion.Play();
        EngineSound.Stop();
        raceMusic.Stop();
        
        Time.timeScale = 0.5f;
        foreach(MeshRenderer m in _player.GetComponentsInChildren<MeshRenderer>())
        {
            m.enabled = false;
        }
        foreach(TrailRenderer t in _player.GetComponentsInChildren<TrailRenderer>())
        {
            t.enabled = false;
        }
        foreach(Light l in _player.GetComponentsInChildren<Light>())
        {
            l.enabled = false;
        }
        foreach(ParticleSystem p in _player.GetComponentsInChildren<ParticleSystem>())
        {
            p.Stop();
        }
        _player.GetComponent<ShipPlayerController>().enabled = false;
        yield return new WaitForSecondsRealtime(2);
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);//this is provisional as it restarts the whole race
    }
}
