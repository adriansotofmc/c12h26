﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minimap : MonoBehaviour
{
    public Transform player;

    public Camera MiniMap;

    public static List<CheckPoint> checkPoints = new List<CheckPoint>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 Position = player.position;
        Position.y = player.position.y + 20;

        transform.position = Position;

        transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y , 0);
    }

    //void DrawIcons()
    //{
    //    Vector3 screenPos = MiniMap 
    //}

    //public void RegisterCheckPoint(GameObject o, Image i)
    //{
    //    Image image = Instantiate(i);
    //    checkPoints.Add(new CheckPoint())
    //}

    //public void RemoveCheckpoints(GameObject o) { }
}
