﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CronometroProvisional : MonoBehaviour
{
    public Text _txtCrono;
    //public IEnumerator myCorru;
    public bool _blnCounting = true;
    public int Minutos = 0;
    public int Segundos = 0;
    public int Milesimas = 0;
    // Start is called before the first frame update
    private void Update()
    {
        if (_blnCounting)
        {
            Cronometrone();
        }
    }
    public void Cronometrone()
    {

        Milesimas += (int)(Time.deltaTime * 1000);
            if (Milesimas > 999)
            {
                Segundos++;
                Milesimas -= 1000;
            }
            if (Segundos > 59)
            {
                Segundos = 0;
                Minutos++;
            }
        _txtCrono.text = $"{Minutos}:{Segundos}:{Milesimas}";
        
    }
}
