﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticClassLoad//next time use a singelton
{
    private static int _intLoadLevelIndex;//you change this value to when you want to change scenes
    private static int _intSceneIndexRetry;//the scene index the retry button has to load

    private static int _intTimeMinutes;
    private static int _intTimeSegundos;
    private static int _intTimeMilesimas;

    //---------------Control options-------------------------//

    private static int _intControllerAxisHorizontal = 1;
    private static int _intControllerAxisVertical = 1;

    //--------------Phantoms and stuff------------------------//

    public static bool blnShowPhantoms;

    public static List<Vector3> PreviousPositions;// the recording of the previous best score
    public static List<Vector3> PreviousRotations;// the recording of the previous best score
    public static List<Vector3> CurrentPositions;//the positions that are currently being recorded
    public static List<Vector3> CurrentRotations;//the positions that are currently being recorded


    public static int RetryButtonIndex
    {
        set
        {
            _intSceneIndexRetry = value;
        }
        get
        {
            return _intSceneIndexRetry;
        }
    }

    public static int AxisHorizontal//-1 or 1
    {
        set
        {
            _intControllerAxisHorizontal = value;
        }
        get
        {
            return _intControllerAxisHorizontal;
        }
    }
    public static int AxisVertical//same
    {
        set
        {
            _intControllerAxisVertical = value;
        }
        get
        {
            return _intControllerAxisVertical;
        }
    }
    public static int LoadIndex
    {
        set
        {
            _intLoadLevelIndex = value;
        }
        get
        {
            return _intLoadLevelIndex;
        }
    }
    public static int Minutes
    {
        get
        {
            return _intTimeMinutes;
        }
    }
    public static int Segundos
    {
        get
        {
            return _intTimeSegundos;
        }
    }
    public static int Milesimas
    {
        get
        {
            return _intTimeMilesimas;
        }
    }
    public static void SetTime(int intMinutes, int intSegundos,int intMilesimas)
    {
        _intTimeMinutes = intMinutes;
        _intTimeSegundos = intSegundos;
        _intTimeMilesimas = intMilesimas;
    }

}
