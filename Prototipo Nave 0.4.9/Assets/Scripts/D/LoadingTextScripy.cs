﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingTextScripy : MonoBehaviour
{

    public string[] TextTips;
    Text txtText;
    // Start is called before the first frame update
    void Start()
    {
        int t = Random.Range(0, TextTips.Length);
        txtText = GetComponent<Text>();
        txtText.text = TextTips[t];

    }
}
