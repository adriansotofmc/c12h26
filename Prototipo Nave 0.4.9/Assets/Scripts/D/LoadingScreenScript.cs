﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenScript : MonoBehaviour
{

    [SerializeField] private Image imLoadingProgress;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadLevel());
        Debug.Log("loading");
    }

    IEnumerator LoadLevel()
    {
        imLoadingProgress.fillAmount = 0.0f;
        yield return new WaitForSeconds(2.0f);//dont use this when the loading screen time increases
        AsyncOperation Loading = SceneManager.LoadSceneAsync(StaticClassLoad.LoadIndex);
        while (!Loading.isDone)
        {
            imLoadingProgress.fillAmount = Mathf.Clamp01(Loading.progress / 0.9f);
            yield return null;
        }
    }
}
