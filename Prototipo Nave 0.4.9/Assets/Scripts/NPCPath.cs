﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCPath : MonoBehaviour
{
    public List<Transform> trmPathPoints;

    private void Start()
    {
        PutNodes();
    }

    public void PutNodes()
    {
        foreach(Transform t in GetComponentInChildren<Transform>())
        {
            if (t != transform)
            {
                trmPathPoints.Add(t);
            }
        }
    }

    private void OnDrawGizmos()
    {
        for(int i = 0; i < trmPathPoints.Count; i++)
        //for(int i = trmPathPoints.Count-1; i >= 0; i--)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(trmPathPoints[i].position, 0.2f);
            Gizmos.color = Color.green;
            if (i + 1 < trmPathPoints.Count)
            {
                Gizmos.DrawLine(trmPathPoints[i].position, trmPathPoints[i + 1].position);
            }
            else
            {
                Gizmos.DrawLine(trmPathPoints[i].position, trmPathPoints[0].position);
            }
        }

    }

}
