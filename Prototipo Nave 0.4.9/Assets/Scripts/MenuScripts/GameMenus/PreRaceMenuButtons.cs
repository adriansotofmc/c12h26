﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PreRaceMenuButtons : MonoBehaviour
{
    GameMaster _gm;
    private void Start()
    {
        _gm = FindObjectOfType<GameMaster>();
    }
    public void StartRaceButton()
    {
        _gm.CountDownEnter();
        Debug.Log("gotem");
    }
    public void Return()
    {
        SceneManager.LoadScene(0);
        Debug.Log("Loading");
    }
}
