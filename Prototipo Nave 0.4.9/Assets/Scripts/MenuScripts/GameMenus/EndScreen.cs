﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
    public Image imLoading;

    public void ReturnToMainMenu()
    {
        
    }
    IEnumerator LoadMainMenu()
    {
        AsyncOperation Loading = SceneManager.LoadSceneAsync(0);
        while (!Loading.isDone)
        {
            imLoading.fillAmount = Mathf.Clamp01(Loading.progress / 0.9f);
            yield return null;
        }
    }
}
