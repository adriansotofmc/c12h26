﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultScreenButton : MonoBehaviour
{
    bool _blnSaved = false;
    public InputField ifPlayerName;

    private void Start()//provisional
    {
        //ScoreBoard.AddTime("D", int.MaxValue, int.MaxValue, int.MaxValue);
    }
    public void MainMenuButton()
    {
        StaticClassLoad.LoadIndex = 0;
        SceneManager.LoadScene("LoadingScene");
    }
    public void RetryButton()
    {
        StaticClassLoad.LoadIndex = StaticClassLoad.RetryButtonIndex;
        SceneManager.LoadScene("LoadingScene");
    }

    public void SaveButton()
    {
        if (!_blnSaved)
        {
            if (ScoreBoard.BetterThanBestTime())
            {
                Debug.Log("Saving Phantom");
                ScoreBoard.BestPosition = StaticClassLoad.CurrentPositions;
                ScoreBoard.BestRotation = StaticClassLoad.CurrentRotations;
                StaticClassLoad.PreviousPositions = StaticClassLoad.CurrentPositions;
                StaticClassLoad.PreviousRotations = StaticClassLoad.CurrentRotations;
            }
            ScoreBoard.AddTime(ifPlayerName.textComponent.text, StaticClassLoad.Minutes, StaticClassLoad.Segundos, StaticClassLoad.Milesimas);
            _blnSaved = true;
        }

        //debug stuff
        Debug.Log(ScoreBoard.ShowTimes().Count+" times saved");
        for(int i = 0; i < ScoreBoard.ShowTimes().Count; i++)
        {
            Debug.Log(ScoreBoard.ShowTimes()[i].strName + "----" + ScoreBoard.ShowTimes()[i].intMinutos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intSegundos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intMilesimas.ToString());        
        }
        if (StaticClassLoad.RetryButtonIndex == 1)
        {
            ScoreBoard.SaveData("Level 2");
        }
        else if (StaticClassLoad.RetryButtonIndex == 2)
        {
            ScoreBoard.SaveData("Level 3");
        }
    }
}
