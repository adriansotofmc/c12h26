﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultScreenScript : MonoBehaviour
{

    public Text txtScoreboard;
    [SerializeField] private Text _txtTime;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1.0f;
        _txtTime.text = $"Time :   {StaticClassLoad.Minutes} : {StaticClassLoad.Segundos} : {StaticClassLoad.Milesimas}";
        if (StaticClassLoad.RetryButtonIndex == 1)
        {
            ScoreBoard.LoadData("Level 2");
        }
        else if(StaticClassLoad.RetryButtonIndex == 2)
        {
            ScoreBoard.LoadData("Level 3");
        }
    }

    private void Update()
    {
        string strScoreboard = "Best Times";

        foreach(SaveFile.PlayerAndScore f in ScoreBoard.ShowTimes())
        {
            strScoreboard += ("\n\n" + f.strName + " -- " + f.intMinutos + " : " + f.intSegundos + " : " + f.intMilesimas);
           // Debug.Log(f.strName + "----" + f.intMinutos.ToString() + ":" + f.intSegundos.ToString() + ":" + f.intMilesimas.ToString());
        }
        txtScoreboard.text = strScoreboard;
    }

}
