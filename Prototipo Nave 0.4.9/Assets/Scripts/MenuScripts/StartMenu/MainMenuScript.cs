﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    CameraScriptMenus myCamera;
    private void Start()
    {
        Time.timeScale = 1.0f;
        myCamera = FindObjectOfType<CameraScriptMenus>();
    }
    public void LevelSelectButton()//this funcion plays an animation so the camera looks at the level select menu
    {
        Debug.Log("UPS");
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        myCamera.ToLevelMenu();
    }
    public void ScoreBoardButton()//the same as before but to the scoreBoard
    {

    }
    public void OptionsButton()//quite redundant isnt it?
    {
        myCamera.ToOptions();
    }
    public void QuitButton()//animation to the quit menu
    {
        //temporal 
        Application.Quit();
    }
}
