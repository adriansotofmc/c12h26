﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class LevelSelectScript : MonoBehaviour
{
    CameraScriptMenus myCamera;
    public Image imLoadingProgress;
    public string strLevel1SceneName;
    public string strLevel2SceneName;
    [Header("BestTimesByLevel")]
    public Text txtBestTimeLevel1;
    public Text txtBestTimeLevel2;
    private void Start()
    {
        myCamera = FindObjectOfType<CameraScriptMenus>();
        SetBestTimes();
    }
    void SetBestTimes()
    {
        try//jajaj 
        {
            if (File.Exists(Application.dataPath + "/" + strLevel1SceneName + ".txt"))
            {
                ScoreBoard.LoadData(strLevel1SceneName);
                txtBestTimeLevel1.text = ScoreBoard.ShowTimes()[0].strName + " " + ScoreBoard.ShowTimes()[0].intMinutos + " : " + ScoreBoard.ShowTimes()[0].intSegundos + " : " + ScoreBoard.ShowTimes()[0].intMilesimas;
            }
            else
            {
                txtBestTimeLevel1.text = "N/A";
            }

        }
        catch
        {
            txtBestTimeLevel1.text = "N/A";
        }

        try
        {
            if (File.Exists(Application.dataPath + "/" + strLevel2SceneName + ".txt"))
            {
                ScoreBoard.LoadData(strLevel2SceneName);
                txtBestTimeLevel2.text = ScoreBoard.ShowTimes()[0].strName + " " + ScoreBoard.ShowTimes()[0].intMinutos + " : " + ScoreBoard.ShowTimes()[0].intSegundos + " : " + ScoreBoard.ShowTimes()[0].intMilesimas;
            }
            else
            {
                txtBestTimeLevel2.text = "N/A";

            }
        }
        catch
        {
            txtBestTimeLevel2.text = "N/A";
        }
    }
    //this two need some kind of transition to the level
    public void Level1Load()//fist level load
    {
        myCamera.ToSelectedLevel();
        StartCoroutine(LoadRace(1));
    }
    public void Level2Load()//second level load
    {
        myCamera.ToSelectedLevel();
        StartCoroutine(LoadRace(2));
    }
    public void ReturnToMainMenu()
    {
        myCamera.ToMainMenuFromLevel();
    }
    IEnumerator  LoadRace(int intScene)
    {
        StaticClassLoad.LoadIndex = intScene;
        AsyncOperation Loading = SceneManager.LoadSceneAsync("LoadingScene");
        while (!Loading.isDone)
        {
            //imLoadingProgress.fillAmount = Mathf.Clamp01(Loading.progress / 0.9f);
            yield return null;
        }
    }
}
