﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuScript : MonoBehaviour
{

    public Image imgCheckerHor;
    public Image imgCheckerVer;
    CameraScriptMenus myCamera;
    private void Start()
    {
        myCamera = FindObjectOfType<CameraScriptMenus>();
        if (StaticClassLoad.AxisHorizontal == 1)
        {
            imgCheckerHor.enabled = false;
        }
        else
        {
            imgCheckerHor.enabled = true;
        }

        if (StaticClassLoad.AxisVertical == 1)
        {
            imgCheckerVer.enabled = false;
        }
        else
        {
            imgCheckerVer.enabled = true;
        }
    }
    public void ReturnButton()
    {
        myCamera.ToMainMenuFromOptions();
    }

    public void InvertX()
    {
        if (StaticClassLoad.AxisHorizontal == 1)
        {
            StaticClassLoad.AxisHorizontal = -1;
            imgCheckerHor.enabled = true;
        }
        else
        {
            StaticClassLoad.AxisHorizontal = 1;
            imgCheckerHor.enabled = false;
        }
    }
    public void InvertY()
    {
        if (StaticClassLoad.AxisVertical == 1)
        {
            StaticClassLoad.AxisVertical = -1;
            imgCheckerVer.enabled = true;
        }
        else
        {
            imgCheckerVer.enabled = false;
            StaticClassLoad.AxisVertical = 1;
        }
    }
    public void FullScreenWindowedButton()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }
}
