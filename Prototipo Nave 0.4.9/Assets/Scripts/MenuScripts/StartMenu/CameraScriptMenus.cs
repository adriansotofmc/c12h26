﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraScriptMenus : MonoBehaviour
{
    public Image imTransitionThing;
    [SerializeField]
    float _fltTransitionTime;
    [Header("List of camera positions")]
    [SerializeField]
    Transform trmMainPos;
    [SerializeField]
    Transform trmLevelPos;
    [SerializeField]
    Transform trmSelectedLevelPos;
    [SerializeField]
    Transform trmOptionsPos;

    private void Start()
    {
        //imTransitionThing.enabled = false;
    }

    public void ToLevelMenu()
    {
        StartCoroutine(LerpToPosition(_fltTransitionTime, trmMainPos, trmLevelPos));
    } 

    public void ToMainMenuFromLevel()
    {
        StartCoroutine(LerpToPosition(_fltTransitionTime, trmLevelPos, trmMainPos));
    }
    public void ToMainMenuFromOptions()
    {
        StartCoroutine(LerpToPosition(_fltTransitionTime, trmOptionsPos, trmMainPos));
    }
    public void ToOptions()
    {
        StartCoroutine(LerpToPosition(_fltTransitionTime, trmMainPos, trmOptionsPos));
    }
    public void ToSelectedLevel()
    {
        StartCoroutine(LerpToPosition(2.0f, trmLevelPos, trmSelectedLevelPos));
        imTransitionThing.enabled = true;
        StartCoroutine(LerpColor(10.0f));
    }

    IEnumerator LerpToPosition(float fltDuration,Transform trmCurrentPos, Transform trmNextPos)
    {
        float fltTime = 0.0f;
        while (fltTime <= fltDuration)
        {
            transform.position = Vector3.Lerp(trmCurrentPos.position, trmNextPos.position, fltTime / fltDuration);
            transform.forward = Vector3.Lerp(trmCurrentPos.forward, trmNextPos.forward, fltTime / fltDuration);
            fltTime += Time.deltaTime;
            yield return null;
        }
    } 
    IEnumerator LerpColor(float fltDuration)
    {
        Color InColor = imTransitionThing.color;
        Color EndColor = new Color(InColor.a, InColor.g, InColor.b, 255);
        float fltTime = 0.0f;
        while (fltTime <= fltDuration)
        {
            imTransitionThing.color = new Color(InColor.a, InColor.g, InColor.b, Mathf.Lerp(0.0f,1.0f,fltTime/fltDuration));
            fltTime += Time.deltaTime;
            yield return null;
        }
    }
}
