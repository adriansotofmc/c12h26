﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartTransitionScript : MonoBehaviour
{
    [SerializeField] Image _imgFader;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FadeInCorru(0.1f));
    }
    private void Update()
    {
        //for debuging only
        //if (Input.GetKeyDown(KeyCode.F10))
        //{
        //    Debug.Log("j");
        //    ScoreBoard.AddTime("pepe", 10, 80, 500);
        //    Debug.Log(ScoreBoard.ShowTimes().Count + " times saved");
        //    for (int i = 0; i < ScoreBoard.ShowTimes().Count; i++)
        //    {
        //        Debug.Log(ScoreBoard.ShowTimes()[i].strName + "----" + ScoreBoard.ShowTimes()[i].intMinutos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intSegundos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intMilesimas.ToString());
        //    }
        //    ScoreBoard.SaveData();
        //}     
        //if (Input.GetKeyDown(KeyCode.F11))
        //{
        //    Debug.Log("j");
        //    ScoreBoard.AddTime("f", 10, 80, 400);
        //    Debug.Log(ScoreBoard.ShowTimes().Count + " times saved");
        //    for (int i = 0; i < ScoreBoard.ShowTimes().Count; i++)
        //    {
        //        Debug.Log(ScoreBoard.ShowTimes()[i].strName + "----" + ScoreBoard.ShowTimes()[i].intMinutos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intSegundos.ToString() + ":" + ScoreBoard.ShowTimes()[i].intMilesimas.ToString());
        //    }
        //    ScoreBoard.SaveData();
        //}
    }

    IEnumerator FadeInCorru(float fltDuration)
    {
        float fltTime = 0.0f;
        while (fltTime < fltDuration)
        {
            fltTime += Time.deltaTime;
            //Debug.Log(fltTime);
            _imgFader.color = new Color(_imgFader.color.r, _imgFader.color.g, _imgFader.color.b, 1 - (fltTime / fltDuration));
            Debug.Log(_imgFader.color);
            yield return null;
        }
        _imgFader.enabled = false;
    }
}
