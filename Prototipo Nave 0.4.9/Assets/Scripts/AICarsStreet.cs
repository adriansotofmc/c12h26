﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
public class AICarsStreet : MonoBehaviour
{

    public NPCPath path;
    public int intCurrentNode;
    CharacterController cc;

    public float fltSpeed;
    public float fltDistanceForArrive;

    // Start is called before the first frame update
    void Start()
    {
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveToNextPoint();
    }

    void MoveToNextPoint()
    {
        transform.forward += (path.trmPathPoints[intCurrentNode].position - transform.position) * 0.2f;
        cc.Move(transform.forward * fltSpeed);
        if ((path.trmPathPoints[intCurrentNode].position - transform.position).sqrMagnitude < fltDistanceForArrive * fltDistanceForArrive)
        {
            intCurrentNode++;
            if(intCurrentNode>= path.trmPathPoints.Count)
            {
                intCurrentNode = 0;
            }
        }
    }
}
