﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blades : MonoBehaviour
{
    [Header("Turbine Blades")]
    public Transform leftBlade;
    public Transform rightBlade;
    public float bladeSpeed = 100.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        leftBlade.Rotate(Vector3.right * (-1) * bladeSpeed * Time.deltaTime);
        rightBlade.Rotate(Vector3.right * bladeSpeed * Time.deltaTime);
    }
}
