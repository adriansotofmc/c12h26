﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarNPC : MonoBehaviour
{
    public Transform[] wayPointList;

    [SerializeField]
    private int currentWayPoint;

    Transform targetWayPoint;

    public float speed = 4f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((currentWayPoint == (this.wayPointList.Length - 1)) || (currentWayPoint > this.wayPointList.Length))
        {
            currentWayPoint = 0;
        }
        targetWayPoint = wayPointList[currentWayPoint + 1];
        transform.forward = Vector3.RotateTowards(transform.forward, targetWayPoint.position - transform.position, speed * Time.deltaTime, 0.0f);
        
        // move towards the target
        transform.position = Vector3.MoveTowards(transform.position, targetWayPoint.position, speed * Time.deltaTime);

        if (transform.position == targetWayPoint.position)
        {
            currentWayPoint++;
            targetWayPoint = wayPointList[currentWayPoint];
        }
    }
}
