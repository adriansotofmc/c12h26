﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ShipPlayerController))]
public class UIController : MonoBehaviour
{
    private ShipPlayerController _Controller;
    private ShipHealth _sh;
    [Header("UI shit")]
    public Image imBoostBar;
    public Image imSpeed;
    public Image imHealth;
    public Text txtSpeed;
    public Text txtBoostEmpty;
    public Image imgFaderSpeed;
    public Text txtCheckpointCount;
    public int intCheckpointNumber;
    public int _intCheckpointCount;

    public Slider sldBoostForRetards;

    bool _blnFlashing = true;//this calue is used to ensure the corrutine only launches one its finished
    bool _blnFlashCooldown = true;//this value ensures that the flash isnt continuos
    // Start is called before the first frame update
    void Start()
    {
        _Controller = GetComponent<ShipPlayerController>();
        _sh = GetComponent<ShipHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        BoostAmoutFuncion();
        SpeedIndicatorFuncion();
        HealthAmountFuncion();
        CheckpointCount();
    }

    void BoostAmoutFuncion()
    {
        imBoostBar.fillAmount = (float)_Controller.fltBoostTime / _Controller.fltMaxBoostTime;
        sldBoostForRetards.value = (float)(_Controller.fltBoostTime / _Controller.fltMaxBoostTime);
        if ((_blnFlashing)&&(_Controller.fltBoostTime==0)&&((_blnFlashCooldown)||(Input.GetButton("Boost"))))
        {
            StartCoroutine(EmptyFlash(0.1f,5));
        }
        if (_Controller.fltBoostTime > 10)
        {
            _blnFlashCooldown = true;
        }
    }
    void CheckpointCount()
    {
        txtCheckpointCount.text = _intCheckpointCount + "/" + intCheckpointNumber;
    }

    void HealthAmountFuncion()
    {
        imHealth.fillAmount = _sh.fltCurrentHealth / _sh.fltMaxHP;
    }
    void SpeedIndicatorFuncion()
    {
        imSpeed.fillAmount = Mathf.Clamp( _Controller.fltSpeed, 0, 1);
        if (_Controller.fltSpeed > 1.0f)
        {
            txtSpeed.color = Color.red;
            txtSpeed.text = "BOOST";
        }
        else
        {
            txtSpeed.color = Color.cyan;
            txtSpeed.text = ((int)(_Controller.fltSpeed * _Controller.fltMaxSpeed)).ToString();
        }
        imgFaderSpeed.color = new Color(imgFaderSpeed.color.r, imgFaderSpeed.color.g, imgFaderSpeed.color.b, _Controller.fltSpeed / 4.0f);
    }
    public IEnumerator EmptyFlash(float fltTime, int intRepeticions)
    {
        _blnFlashing = false;
        _blnFlashCooldown = false;
        int intLoops = 0;
        while (intLoops < intRepeticions)
        {
            var fltSteps = 0.0f;
            txtBoostEmpty.text = "EMPTY";
            while (fltSteps < fltTime)
            {
                txtBoostEmpty.color = Color.Lerp(new Color(1, 0, 0, 0), new Color(1, 0, 0, 1), fltSteps);
                fltSteps += Time.deltaTime;
                yield return null;
            }
            fltSteps = 0.0f;
            while (fltSteps < fltTime)
            {
                txtBoostEmpty.color = Color.Lerp(new Color(1, 0, 0, 1), new Color(1, 0, 0, 0), fltSteps);
                fltSteps += Time.deltaTime;
                yield return null;
            }
            intLoops++;
        }
        txtBoostEmpty.color = new Color(1, 0, 0, 0);
        _blnFlashing = true;
        yield return null;
    }
    public IEnumerator FaderEnd(float fltTime)//half way there
    {
        float fltF = 0.0f;
        while (fltF < fltTime)
        {
            fltF += Time.deltaTime;
            yield return null;
        }
    }
}
