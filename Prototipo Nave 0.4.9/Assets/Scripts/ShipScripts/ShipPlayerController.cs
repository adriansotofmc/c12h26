﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]
public class ShipPlayerController : MonoBehaviour//some of the code in here is dependent in frame rate so we should limit that to a dertain value
{


    [Header("Control settings(Maybe usefull if we put it in the pause menu)")]
    [Range(-1,1)]
    public int intXInversion;
    [Range(-1,1)]
    public int intYInversion;
    [Header("Current values")]
    [Tooltip("porcentage of the maximus speed")]
    [Range(0.2f,1)]
    public float fltSpeed;// porcentage of the maximun speed at the given momnet
    [Range(-1,1)]
    public float fltStrafeSpeed;// porcentage of strafe speed

    [Header("Movement Settings")]
    public float fltMaxSpeed;//MAX Speed
    public float fltTurnForce;//MAX trunnig force when trunnig
    public float fltAcceleration;//Acceleration
    public float fltDecceleration;//stopingForce
    public float fltMaxStrafeSpeed;//Max strafeSpeed
    public float fltAngle;//angle when rotating
    public float fltStrafeAcceleration;//strafe acceleration
    public float fltForceSpeedRatio;//ratio between speed and turnforce
    public float fltRestingSpeed;//lower Speed than max
    public float fltRestingAccel;//decress in speed rate
    public float fltGravityAccel;
    [Range(0.01f,1.0f)]
    public float fltMinimunSpeed;
    [Header("Boost")]
    public float fltBoost;//float that goes in speed during boost
    public float fltBoostAccel;//boost acceleration
    public float fltMaxBoostTime;
    [Range(0,1000)]
    public float fltBoostTime;//boost limiter
    public float fltDistanceChargeBottom;//max distance it checks for colision to recharge boost
    public float fltDistanceChargeSides;
    public float fltBoostRechargeRate;//recharge rate
    public float fltBoostDecreaseRate;
    public ParticleSystem psBottom;
    public ParticleSystem psRight;
    public ParticleSystem psLeft;
    TrailRenderer[] trTrails;
    [Header("SideDash")]
    public float fltDashSpeed;
    public float fltDashDuration;
    public float fltDashCooldown;
    bool blnDashAvailable = true;
    [Header("Camera Settings??")]
    public float fltCameraExageration;

    [Header("Feel settings?")]
    public float fltShipShake;

    [Header("Dont Touch Yet")]
    public Transform ShipTransform;//the transform of the mesh of the ship
    public Transform trCamera;//so far only used for camera exageration---------if cinemachine Change
    //private variables
    CharacterController _chcrController;
    Camera _cmCamera;//--------------------------------------------------------if cinemachine Change
    public PostProcessProfile _postProcess;/////////////// Work in progress /////////////////////////

    float _fltVer = 0;
    float _fltHor = 0;


    public bool blnAuxiliarForAchivements = false;

    public enum States
    {
        Pause,
        Stopped,
        DuringCountDown,
        Normal,
        SideDash,
        Boost
    }

    public States ShipState = States.Normal;

    // Start is called before the first frame update
    void Start()
    {
        //change the inputs the same way as the options say

        intXInversion = StaticClassLoad.AxisVertical;
        intYInversion = StaticClassLoad.AxisHorizontal;
        _chcrController = GetComponent<CharacterController>();
        //be carefull with this if we use cinemachine
        _cmCamera = GetComponentInChildren<Camera>();
        
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
        trTrails = GetComponentsInChildren<TrailRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        switch (ShipState)
        {
            case States.Stopped:
                break;
            case States.DuringCountDown:
                ForwardMovementFuncion();
                BoostRechargeFuncion();
                break;
            case States.Pause:
                ForwardMovementFuncion();
                BoostRechargeFuncion();
                break;
            case States.Normal:
                //movements funcions
                GravityAccelFuncion();//this must be placed before the input funcion
                InputFuncion();
                ForwardMovementFuncion();
                DirectionFuncion();
                //feel funcions
                ShipNoiseFuncion(fltSpeed * fltShipShake);
                TrailsColorFuncion();
                break;
            case States.SideDash:
                //movemets funcion
                GravityAccelFuncion();
                ForwardMovementFuncion();
                //feel funcions
                ShipNoiseFuncion(fltSpeed * fltShipShake);
                TrailsColorFuncion();
                break;
            case States.Boost:
                InputFuncion();
                ForwardMovementFuncion();
                DirectionFuncion();
                //while in this you boost
                BoostFuncion();
                //feel funcions
                ShipNoiseFuncion(fltSpeed * fltShipShake);
                TrailsColorFuncion();
                break;
        }
    }

    void InputFuncion()//gets inputs//manages boost activation and acceleration
    {
        if (((Input.GetKey(KeyCode.Space) || (Input.GetButton("Boost")))) && (fltBoostTime > 0))//ask if boost
        {
            ShipState = States.Boost;
        }
        else
        {
            ShipState = States.Normal;
            //ShipState = States.Normal;//this may conflic with other parts of the stateMachine in the future
            if (!(Input.GetKey(KeyCode.Space) || (ShipState==States.Boost)))//if you boost while grinding on a surface and you get away from said surface the particle effects dont deactivate
            {
                BoostRechargeFuncion();
            }
            if ((fltSpeed > 1)&&(ShipState!=States.Boost))
            {
                fltSpeed -= fltBoostAccel;
            }
            else
            {                                //// the faster you go the less you accelerate
                if (Input.GetAxis("Accel") > 0 )
                {
                    fltSpeed += fltAcceleration / (fltSpeed * 7.5f) * Input.GetAxis("Accel"); // Allow the player to control accelerate
                }
                else
                {
           
                    fltSpeed += fltDecceleration * Input.GetAxis("Accel"); // Allow the player to control accelerate

                }
                //fltSpeed -= fltAcceleration * Input.GetAxis("Deccel")*10;  // Allow the player to control accelerate

                if (Input.GetKey(KeyCode.LeftControl))
                {
                    fltSpeed -= fltAcceleration / (fltSpeed * 7.5f); // Allow the player to control accelerate
                }
                else if (Input.GetKey(KeyCode.LeftShift))
                {
                    fltSpeed += fltDecceleration; // Allow the player to control accelerate

                }

                fltSpeed = Mathf.Clamp(fltSpeed, fltMinimunSpeed, 1);
                if ((fltSpeed > fltRestingSpeed)&&(Input.GetAxis("Accel")==0))
                {
                    
                    fltSpeed -= fltRestingAccel;
                }

            }
        }
        //post process shit
        if (Input.GetAxis("Horizontal") == 0)
        {
            fltStrafeSpeed -= fltStrafeAcceleration * fltStrafeSpeed;
            // fltStrafeSpeed = 0;
        }
        fltStrafeSpeed += fltStrafeAcceleration * Input.GetAxis("Horizontal") * intYInversion;
        fltStrafeSpeed = Mathf.Clamp(fltStrafeSpeed, -1, 1);
    }
    void DirectionFuncion()//changes direcction of ship
    {
        //this is the mouse one
        var _fltx = _fltHor;
        var _flty = _fltVer;    //guardar los valores anteriores

        //this is the controller one
        //if (fltSpeed <= 0.25)//gravity affects you at low speeds
        //{
        //    if (_fltHor < 50)//not specially liking the feel of this
        //    {
        //        _fltHor += (fltTurnForce / 2) * Time.deltaTime * (1.2f - fltSpeed);
        //    }
        //}
        //People didnt like this
        _fltHor += (intXInversion * fltTurnForce * Input.GetAxis("Vertical") * Time.deltaTime) * (1.2f - fltSpeed * fltForceSpeedRatio);
        _fltHor = Mathf.Clamp(_fltHor, -90, 90); //limit camera angles
        _fltVer += (intYInversion * fltTurnForce * Input.GetAxis("Horizontal") * Time.deltaTime) * (1.2f - fltSpeed * fltForceSpeedRatio);



        transform.rotation = Quaternion.Euler(_fltHor, _fltVer, 0.0f);
        ShipTransform.rotation = transform.rotation * Quaternion.Euler((_fltHor-_fltx)*0.8f*fltSpeed,(_fltVer-_flty)*0.8f*fltSpeed, fltAngle * -fltStrafeSpeed);//the ship does the thing//modified
        
        //camera exageration (find better ways of doing this)-------------change with cinemachine
       // trCamera.rotation = transform.rotation * Quaternion.Euler((_fltx-_fltHor) * fltCameraExageration * fltSpeed, (_flty-_fltVer) * fltCameraExageration * fltSpeed, 0.0f);

    }

    void ForwardMovementFuncion()//moves the ship forward
    {

        _chcrController.Move(transform.forward * fltSpeed * fltMaxSpeed * Time.deltaTime);// i move the player forward all the time
        //this might need to be changed if we use cinemachine
        if (fltSpeed < fltBoost+0.001f)
        {
            _cmCamera.fieldOfView = 60 + (fltSpeed * 20);//this is where the fov is changed---------change with cinemachine
        }
        else
        {
            _cmCamera.fieldOfView = 60 + (fltBoost * 20);//this is where the fov is changed---------change with cinemachine

        }
    }
    
    public void BoostFuncion()//makes ship go fiuuuu
    {
        if (fltSpeed < fltBoost)
        {
            fltSpeed += fltBoostAccel;
        }
        fltBoostTime -= fltBoostDecreaseRate * Time.deltaTime;
    }

    void BoostRechargeFuncion()//recharges boost ///halfway there oO //bugged when speed low ti recharges the boost amount//bug is no more//bug where the particle effects dont disapear after getting away from a wall
    {
        blnAuxiliarForAchivements = false;
        RaycastHit hit;
        var layerMask = 1 << 8;
        layerMask = ~layerMask;
        if ((Physics.Raycast(transform.position, -ShipTransform.up, out hit, fltDistanceChargeBottom,layerMask)))
        {
            blnAuxiliarForAchivements = true;
            if (fltBoostTime < fltMaxBoostTime)
            {
                fltBoostTime += fltBoostRechargeRate * Time.deltaTime;
            }
            if (!psBottom.isPlaying)
            {
                psBottom.Play();
            }
            psBottom.gameObject.transform.position = hit.point + new Vector3(0, 0.1f, 0);
        }
        else
        {
            psBottom.Stop();
        }
        if ((Physics.Raycast(transform.position, ShipTransform.right, out hit, fltDistanceChargeSides,layerMask)))
        {
            if (fltBoostTime < fltMaxBoostTime)
            {
                blnAuxiliarForAchivements = true;
                fltBoostTime += fltBoostRechargeRate * Time.deltaTime;
            }
            if (!psRight.isPlaying)
            {
                psRight.Play();
            }
            psRight.gameObject.transform.position = hit.point + Vector3.left * 0.1f;

        }
        else
        {
            psRight.Stop();
        }
        if ((Physics.Raycast(transform.position, -ShipTransform.right, out hit, fltDistanceChargeSides,layerMask)))
        {
            if (fltBoostTime < fltMaxBoostTime)
            {
                blnAuxiliarForAchivements = true;
                fltBoostTime += fltBoostRechargeRate * Time.deltaTime;
            }
            if (!psLeft.isPlaying)
            {
                psLeft.Play();
            }
            psLeft.gameObject.transform.position = hit.point + Vector3.right * 0.1f;
        }
        else
        {
            psLeft.Stop();
        }
    }
    void ShipNoiseFuncion(float fltRange)//makes the ship vibrate with speed
    {
        ShipTransform.position = transform.position + new Vector3(Random.Range(-fltRange,fltRange), Random.Range(-fltRange,fltRange), Random.Range(-fltRange,fltRange));
    }
    
    void TrailsColorFuncion()//trails change colors when boost
    {
        foreach(TrailRenderer t in trTrails)
        {
            //t.startColor = Color.Lerp(Color.cyan, Color.red, Mathf.Clamp(fltSpeed - 1, 0, 1));
        }
    }

    void GravityAccelFuncion()
    {
        if ((fltSpeed < 1.0f)&&(fltSpeed>=fltMinimunSpeed))
        {
            fltSpeed += fltGravityAccel * (transform.rotation.x / 90);
        }  
    }

    public void ChecpointRestart(Vector3 v3Posicion, Quaternion qRotacion)//for checkpoints to use
    {
        transform.position = v3Posicion;
        _fltHor = qRotacion.eulerAngles.x;
        _fltVer = qRotacion.eulerAngles.y;
        fltSpeed = 0.3f;
    }
    
    IEnumerator DashCorroutine(int intSide)
    {
        Quaternion InitialAngle = Quaternion.Euler(0.0f, 0.0f, ShipTransform.rotation.eulerAngles.z);
        blnDashAvailable = false;
        ShipState = States.SideDash;
        float fltCounter = 0;
        float fltFraccion = 0;
        while (fltCounter < fltDashDuration)
        {
            if (fltFraccion < 1.2)
            {
                fltFraccion = ((fltCounter * 1.2f) / fltDashDuration);
            }
            else
            {
                fltFraccion -= fltCounter;
            }
            //fltFraccion = Mathf.Clamp(fltFraccion, 0, 1);
            _chcrController.Move(transform.right * intSide * fltDashSpeed * (fltSpeed / 1) * Time.deltaTime);
            ShipTransform.rotation = transform.rotation * InitialAngle *Quaternion.Euler(0.0f, 0.0f, -intSide * Mathf.Lerp(0, 360, fltFraccion));//the ship does the thing//modified
            yield return null;
            fltCounter += Time.deltaTime;
        }
        ShipState = States.Normal;
        StartCoroutine(DashCooldownCorroutine());
    }

    IEnumerator DashCooldownCorroutine()
    {
        float fltCounter = 0;
        while (fltCounter < fltDashCooldown)
        {
            yield return null;
            fltCounter += Time.deltaTime;
        }
        blnDashAvailable = true;
    }

    private void OnDrawGizmos()//drawing the gizmos for boost recharge
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position - (ShipTransform.up * fltDistanceChargeBottom));
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + (ShipTransform.right * fltDistanceChargeSides));
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position - (ShipTransform.right * fltDistanceChargeSides));
    }

}
