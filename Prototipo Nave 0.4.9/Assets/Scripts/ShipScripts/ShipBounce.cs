﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBounce : MonoBehaviour
{

    [Header("Force Stats")]
    public float fltBounceForce;
    public float fltBounceDecay;

    ShipPlayerController _spcPlayer;
    Vector3 v3BounceSpeed = new Vector3(0, 0, 0);
    CharacterController _ccPlayer;

    private void Start()
    {
        _ccPlayer = GetComponent<CharacterController>();
        _spcPlayer = GetComponent<ShipPlayerController>();
    }

    private void Update()
    {
        if (v3BounceSpeed.magnitude > 0)
        {
            ForceApply();
            ForceDecay();
        }
    }

    void ForceApply()
    {
        _ccPlayer.Move(v3BounceSpeed);
    }
    void ForceDecay()
    {
        //v3BounceSpeed -= v3BounceSpeed.normalized * fltBounceDecay * Time.deltaTime;
        v3BounceSpeed -= v3BounceSpeed * fltBounceDecay * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag != "Player")
        {
            Debug.Log($"Collision{collision.collider.name}");
            //v3BounceSpeed = collision.GetContact(0).normal * (fltBounceForce);//añadir angulo con la velocidad y la velocidad en si
            v3BounceSpeed = collision.GetContact(0).normal * (fltBounceForce * (90.0f / Vector3.Angle(transform.forward, collision.GetContact(0).normal)) - 1.0f);//dont really know if anything changed
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.tag != "Player")
        {
            Debug.Log($"Collision{collision.collider.name}");
            //v3BounceSpeed = collision.GetContact(0).normal * (fltBounceForce);//añadir angulo con la velocidad y la velocidad en si
            v3BounceSpeed = collision.GetContact(0).normal * (fltBounceForce * (90.0f / Vector3.Angle(transform.forward, collision.GetContact(0).normal)) - 1.0f);//dont really know if anything changed
        }
    }
}
