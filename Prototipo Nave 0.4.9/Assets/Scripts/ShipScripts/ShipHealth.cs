﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipHealth : Health
{
    [Header("stats")]
    [Range(-90.0f,90.0f)]
    public float fltDamageAngle;
    public float fltDamage;
    [Header("Do not even think about it :)")]
    public GameMaster _gm;
    private Rigidbody rg;
    private ShipPlayerController spc;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        rg = GetComponentInChildren<Rigidbody>();
        spc = GetComponent<ShipPlayerController>();
    }
    // Update is called once per frame
    public override void ChangeHP(float fltAddValue)
    {
        base.ChangeHP(fltAddValue);
        if (fltCurrentHealth <= 0)
        {
            PlayerDead();//the player fucking dies
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.name != "Player" || (collision.collider.gameObject.tag != "Bullet" && collision.collider.tag != "Player"))
        {
            Debug.Log("Collision");
            Debug.Log(collision.collider.gameObject.name);
            Vector3 v3Dirrec = spc.transform.forward;
            Vector3 v3NormalWall = collision.contacts[0].normal;
            if (Vector3.Angle(v3Dirrec, -v3NormalWall) > fltDamageAngle)//this isnt working like its suposed to pls fix this shit
            {
                Debug.Log("TakingDamage");
                ChangeHP(-fltDamage * 90 / Vector3.Angle(v3Dirrec, -v3NormalWall) * Time.deltaTime);//the angle of collision doesnt matter
            }
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.collider.gameObject.name != "Player" || (collision.collider.gameObject.tag != "Bullet" && collision.collider.tag != "Player"))
        {
            Debug.Log("Collision");
            Debug.Log(collision.collider.gameObject.name);
            Vector3 v3Dirrec = spc.transform.forward;
            Vector3 v3NormalWall = collision.contacts[0].normal;
            if (Vector3.Angle(v3Dirrec, -v3NormalWall) > fltDamageAngle)//this isnt working like its suposed to pls fix this shit
            {
                Debug.Log("TakingDamage");
                ChangeHP(-fltDamage * 90 / Vector3.Angle(v3Dirrec, -v3NormalWall) * Time.deltaTime);//the angle of collision doesnt matter
            }
        }
    }

    void PlayerDead()
    {
        _gm.RestartOnDead();
    }
}
