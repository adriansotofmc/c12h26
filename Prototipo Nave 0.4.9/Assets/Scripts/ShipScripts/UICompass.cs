﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICompass : MonoBehaviour
{
    public Transform trmNextCheckpoint;
    [SerializeField] Text _txtDistanceToCheckpoint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Lookat();
        UpdateDistance();
    }

    void Lookat()
    {
        //transform.forward = (trmNextCheckpoint.position - gameObject.GetComponentInParent<Transform>().position).normalized;
        transform.forward += (trmNextCheckpoint.position - gameObject.GetComponentInParent<Transform>().position).normalized * Time.deltaTime * 5;
        transform.forward = new Vector3(transform.forward.x, 0.0f, transform.forward.z);
    }

    void UpdateDistance()
    {
        Vector3 _v3CheckP = new Vector3(trmNextCheckpoint.position.x, 0.0f, trmNextCheckpoint.position.z);
        Vector3 _v3tPos = new Vector3(gameObject.GetComponentInParent<Transform>().position.x, 0.0f, gameObject.GetComponentInParent<Transform>().position.z);
        //_txtDistanceToCheckpoint.text = $"{(int)Vector3.Distance(trmNextCheckpoint.position, gameObject.GetComponentInParent<Transform>().position)} m";
        _txtDistanceToCheckpoint.text = $"{(int)Vector3.Distance(_v3CheckP,_v3tPos)} m";
    }

    public void SetTargetCheckpoint(Transform trnT)
    {
        trmNextCheckpoint = trnT;
    }
}
