﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSky : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform deathSky;
    public Transform target;
    [SerializeField]
    private float speed = 6.5f;
    public bool _blnActive;

    void Start()
    {
        _blnActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_blnActive)
        {
            float step = speed * Time.deltaTime;
            deathSky.position = Vector3.MoveTowards(transform.position, target.position, step);
        }

    }
}
